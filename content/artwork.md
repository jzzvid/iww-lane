# Artwork

This page is dedicated to featuring IWW artwork including images, pamphelets, and posters both past and modern.  This page serves as both a resource center for activists and as a means of documenting and preserving our history.

## Index
- ["80 Years of Rebel Art".](http://www.ridetoride.net/iwwlane/80-Years/index.html)  From the "Travelling IWW Art Show" in 1985.
- [Posters.](#posters)  Featured posters.
- [Stickers, & Decals.](#decals)  Make an impression, spread the word!
- [Images.](#images)  Other various posters, art, images, and memes.
- [Historical.](#history)  Historical artwork.  (Coming soon...)

## <a name="posters"></a> Posters
Coming soon!

## <a name="decals"> </a> Stickers & Decals
One Big Union Pennant [PDF]: </br>
[![Pennant](/iww-lane/img/art/OneBigUnionFlag_2sided-200.jpg)](/downloads/OneBigUnionFlag_2sided.pdf)

One Big Union Banner, Red [PDF]: </br>
[![Red Banner](/iww-lane/img/art/OneBigUnion_banner_red-200.jpg)](/downloads/OneBigUnion_banner_red.pdf)

One Big Union Banner, Black [PDF]: </br>
[![Red Banner](/iww-lane/img/art/OneBigUnion_banner_redblck-200.jpg)](/downloads/OneBigUnion_banner_redblck.pdf)

More Time for Surfing [JPG]: </br>
![Surf](/iww-lane/img/art/better_surf-200.jpg)

## <a name="images"> </a> Images
![Soapbox](/iww-lane/img/art/sis_geb_200.jpg)

## <a name="history"> </a> Historical
Coming Soon!
