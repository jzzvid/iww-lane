# Events
The IWW of Lane County hosts two regular events a month:

- **Movie Night:**  First Wednesday of the Month, 7:00 PM:  McNail-Riley House, 601 W. 13th Ave., Eugene, OR.
- **IWW Monthly Meeting:**  Second Tuesday of the Month, 6:30 PM - 8:00  PM:  New Day Bakery, 449 Blair Blvd., Eugene, OR.

Upcoming Events:

- **Saturday Market:**  13 October 2018, 9:30 AM - 5:00 PM:  126 E. 8th Ave., Eugene, OR.

For other events, including community events, check our blog posts or our Facebook page.
