# About Us
## Who are we?
We are the Lane County, Oregon General Member Branch (Lane GMB) of the International Workers of the World (IWW), a local chapter of the world's oldest active labor union.  We are dedicated to the cause of assisting workers in organizing the workplace, informing workers of their rights, and working to defend workers against unfair and illegal labor practices.

The current incarnation of the Lane GMB was established in 2006.  We work with other organizations such as the [Eugene-Springfield Solidarity Network](https://www.facebook.com/groups/166453220083281/about/) to further our mission of Solidarity amoung the working class.

The IWW was founded in 1905 in Chicago, IL and is the oldest active workers union in the United States and the first union to unite all trades.  For over a century we have fought to bring such radical ideas to the forefrount such as putting an end to dangerous child labor practices, promoting the 40-hour week, the weekend, the creation of safe labor practices and standards, wrongful termination laws, the Americans with Disabilities Act, the right to strike, and the ending of sweatshops in America.

Unlike other unions, the IWW believes in direct action and community involvement above politics.  As such, the IWW does not endorse candidates.  We will never tell our members who to vote for and their dues will never go to funding any political party or candidate.  This is our promise to our members that the dues they pay will go towards organizing their workplace or community projects voted on by our members.

## Why join the IWW?
The IWW continues in their mission of worker solidarity.  We have fought long for the rights of workers throughout the decades, but our work is far from over — especially in these times.  The working class has a long road ahead of us if we are to ensure our rights and prosperity.

By joining the IWW you becomes a part of a movement which seeks to further our common struggle for fair compensation, ensuring workplace safety, ensuring that workplaces hear the grievences of their employees, and standing up for the rights of all workers so that no one is treated unjustly.  As part of the IWW you will also learn what you can do not only at your workplace, but also in your community to help those in need.
 
## What you can do?
The easiest way to get involved is to come to our meetings.  There you will learn about all of the opportunities the Lane GMB provides for helping fellow workers.  At the meetings we discuss how to properly organize, issue collective grievences against your employer, and other tactics.  We also work with ot her organizations to provide periodic "Know Your Rights" classes and campaigns.

At these meetings also work with other local charities and groups, so we are a great nexus point to promote awareness of community events.  We also discuss the current challenges facing the working-class and what we can do as workers and as members within our community to both raise awareness and help solve these issues.


## How do I qualify?
All members of the working-class may join the IWW with exception to those involved in hiring decisions ("bosses") or those involved in law enforcement ("cops").  If you wish to contribute to the cause of the IWW but do not qualify for membership you are welcome to make a donation.

Membership requires the payment of mothly dues and an initiation fee which can be paid either online anytime via PayPal or at our monthly meetings.
